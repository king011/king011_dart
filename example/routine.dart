import 'dart:async';

import 'package:king011_dart/king011_dart.dart';

class Delayed<T> implements Routine {
  Duration duration;
  T value;
  Delayed({required this.duration, required this.value});
  @override
  Future<T> done() {
    print('done $value');
    return Future.delayed(duration, () {
      print(value);
      return value;
    });
  }
}

void main() async {
  /// 同步例程
  Scheduler scheduler = SyncScheduler();
  scheduler.addAll([
    Delayed(
      duration: Duration(seconds: 1),
      value: 1,
    ),
    Delayed(
      duration: Duration(seconds: 1),
      value: 'ok',
    ),
    Delayed(
      duration: Duration(seconds: 1),
      value: 'cat',
    ),
  ]);
  await scheduler.done();

  /// 異步例程
  scheduler = AsyncScheduler();
  scheduler.addAll([
    Delayed(
      duration: Duration(seconds: 1),
      value: 1,
    ),
    Delayed(
      duration: Duration(milliseconds: 500),
      value: 'ok',
    ),
    Delayed(
      duration: Duration(seconds: 2),
      value: 'cat',
    ),
  ]);
  await scheduler.done();
}
