import 'dart:async';

import 'package:king011_dart/king011_dart.dart';

void main() {
  final completers = Completers<bool>();
  var c0 = Completer<bool>();
  var c1 = Completer<bool>();
  c0.future.then((ok) => print('c0 $ok'));
  c1.future.then((ok) => print('c1 $ok'));

  completers
    ..add(c0)
    ..add(c1)
    ..complete(true);

  c0 = Completer<bool>();
  c1 = Completer<bool>();
  c0.future.catchError((e) {
    print('c0 $e');
    return true;
  });
  c1.future.catchError((e) {
    print('c1 $e');
    return true;
  });
  completers
    ..add(c0)
    ..add(c1)
    ..completeError('test error');
}
