import 'dart:async';

import './future.dart';

abstract class Routine<T> {
  Future<T> done();
}

/// 返回 true 表示 錯誤已經被處理 Scheduler 將繼續執行後續 操作 否則 停止 Scheduler
typedef OnErrorCallback = bool Function(Routine routine, dynamic e);

class Scheduler<T> implements Routine {
  final List<Routine> _routines = [];
  final _completers = Completers<bool>();
  bool get isRunning => _completers.isNotEmpty;

  OnErrorCallback? onError;
  Scheduler({this.onError});

  /// 添加 例程
  bool add(Routine routine) {
    if (_completers.isNotEmpty) {
      return false;
    }
    _routines.add(routine);
    return true;
  }

  /// 添加 例程
  bool addAll(Iterable<Routine> iterable) {
    if (_completers.isNotEmpty || iterable.isEmpty) {
      return false;
    }
    _routines.addAll(iterable);
    return true;
  }

  @override
  Future<bool> done() {
    return Future.value(true);
  }
}

/// 同步執行的調度器 其中的例程 將依次執行
class SyncScheduler extends Scheduler {
  SyncScheduler({OnErrorCallback? onError}) : super(onError: onError);
  @override
  Future<bool> done() {
    if (_routines.isEmpty) {
      return Future.value(true);
    }

    final completer = Completer<bool>();
    _completers.add(completer);
    if (_completers.length == 1) {
      _done();
    }
    return completer.future;
  }

  void _done() async {
    for (var i = 0; i < _routines.length; i++) {
      try {
        await _routines[i].done();
      } catch (e) {
        if (onError == null || !onError!(_routines[i], e)) {
          _completers.complete(false);
        }
      }
    }
    _completers.complete(true);
  }
}

/// 異步執行的調度器 其中的例程 將同時執行
class AsyncScheduler extends Scheduler {
  AsyncScheduler({OnErrorCallback? onError}) : super(onError: onError);

  /// 全部執行成功 才返回 true 否則 返回 false 不會拋出異常
  @override
  Future<bool> done() {
    if (_routines.isEmpty) {
      return Future.value(true);
    }
    final completer = Completer<bool>();
    _completers.add(completer);
    if (_completers.length == 1) {
      _done();
    }
    return completer.future;
  }

  void _done() {
    var wait = 0;
    var ok = true;
    _routines.forEach((v) {
      ++wait;
      v.done().catchError((e) {
        if (onError == null || !onError!(v, e)) {
          if (ok) {
            ok = false;
          }
        }
      }).whenComplete(() {
        --wait;
        if (wait == 0) {
          _completers.complete(ok);
        }
      });
    });
  }
}
