import 'dart:async';

class Completers<T> {
  final _completers = <Completer<T>>[];
  int get length => _completers.length;
  bool get isNotEmpty => _completers.isNotEmpty;
  bool get isEmpty => _completers.isEmpty;
  void add(Completer<T> value) {
    _completers.add(value);
  }

  void complete(T value) {
    switch (_completers.length) {
      case 0:
        return;
      case 1:
        _completers[0].complete(value);
        break;
      case 2:
        _completers[0].complete(value);
        _completers[1].complete(value);
        break;
      case 3:
        _completers[0].complete(value);
        _completers[1].complete(value);
        _completers[2].complete(value);
        break;
      default:
        _completers.forEach((completer) => completer.complete(value));
    }
    _completers.clear();
  }

  void completeError(Object error, [StackTrace? stackTrace]) {
    switch (_completers.length) {
      case 0:
        return;
      case 1:
        _completers[0].completeError(error, stackTrace);
        break;
      case 2:
        _completers[0].completeError(error, stackTrace);
        _completers[1].completeError(error, stackTrace);
        break;
      case 3:
        _completers[0].completeError(error, stackTrace);
        _completers[1].completeError(error, stackTrace);
        _completers[2].completeError(error, stackTrace);
        break;
      default:
        _completers
            .forEach((completer) => completer.completeError(error, stackTrace));
    }
    _completers.clear();
  }
}
