import 'dart:async';

import 'package:king011_dart/king011_dart.dart';
import 'package:test/test.dart';

void main() {
  group('future of tests', () {
    test('Completers Test', () {
      final completers = Completers<bool>();
      var c0 = Completer<bool>();
      var c1 = Completer<bool>();
      var v = 0;
      c0.future.then((ok) => ++v);
      c1.future.then((ok) => ++v).whenComplete(() {
        expect(v, 2);
      });
      expect(completers.isEmpty, true);
      completers
        ..add(c0)
        ..add(c1);
      expect(completers.length, 2);
      completers.complete(true);
      expect(completers.isEmpty, true);

      c0 = Completer<bool>();
      c1 = Completer<bool>();
      c0.future.catchError((e) {
        --v;
        return true;
      });
      c1.future.catchError((e) {
        --v;
        return true;
      }).whenComplete(() {
        expect(v, 0);
        return true;
      });
      completers
        ..add(c0)
        ..add(c1);
      expect(completers.length, 2);
      completers.completeError('test error');
      expect(completers.isEmpty, true);
      expect(true, true);
    });
  });
}
